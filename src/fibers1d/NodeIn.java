package fibers1d;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeIn extends NodeBox {
  /* ****************************************************** */
  @Override public void Pass_Back_Corrector() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.Apply_Height();
    }
  }
  /* ****************************************************** */
  @Override public void Ping() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      this.planeform.Ping(cpnt);
    }
  }
  /* ****************************************************** */
  @Override public NodeIn Clone_Me() {
    NodeIn child = new NodeIn();
    return child;
  }
}
