package fibers1d;

import static fibers1d.Things.BoxSpacing;

/**
 *
 * @author MultiTool
 *
 * The purpose of this test is to try out motes in absolute linear (raw) space,
 * and where the output fire values of those motes are mapped through the ActFun
 * curve.
 *
 * The plane is fit directly to those raw mote values, and the motes are fit to
 * the raw linear plane.
 *
 * Seems equal to motes being in post-curve space, not sure which is better.
 *
 */
/* ****************************************************** */
public class NodeTest1 extends NodeBox {// stand alone bench test
  int Num_Dims = 3;
  /* ****************************************************** */
  public void Test() {
    this.Init_States(4);// must rethink this
    this.xorg = (1.0) * BoxSpacing;
    this.yorg = (1.0) * BoxSpacing;
    this.Init_And();
//    this.Init_Or();
//    this.Init_Same(-1.0);
//    this.Init_Same(1.0);
    this.planeform.Clear();
    for (int cnt = 0; cnt < 100; cnt++) {
      this.Ping();
      this.FitError = this.GetFitError();
      PrintPlane();
    }
  }
  /* ****************************************************** */
  @Override public void Init_And() {
    this.Explodable = false;
    double amp = 1.99, andfloat;// note: just making amp huge has same effect as Reverse_ActFun, of course. 
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int andval = (pcnt & 1) & ((pcnt >> 1) & 1);
      andfloat = (((double) andval) - 0.5) * amp;
      andfloat = Globals.Reverse_ActFun(andfloat);
      System.out.println("andfloat:" + andfloat);
      cpnt.loc[cpnt.ninputs] = andfloat;
    }
  }
  /* ****************************************************** */
  public void PrintPlane() {// find the heights of the corners of the plane
    System.out.println("Plane");
    double PlaneHgt;
    Mote cpnt = new Mote(this, Num_Dims);
    for (int ycnt = 0; ycnt < 2; ycnt++) {
      cpnt.loc[1] = (ycnt * 2.0) - 1.0;
      for (int xcnt = 0; xcnt < 2; xcnt++) {
        cpnt.loc[0] = (xcnt * 2.0) - 1.0;
        PlaneHgt = this.Get_Plane_Sigmoid_Height(cpnt);
        System.out.print(PlaneHgt + ", ");
      }
    }
    System.out.println();
  }
  @Override public void Collect_And_Fire() {
    int StateCnt, NumStates = this.Motes.size();
    int DimCnt, NumDims = this.US.size();
    double FireVal;
    Axon link;
    Mote mote;
    NodeBox usnode;
    for (StateCnt = 0; StateCnt < NumStates; StateCnt++) {
      mote = this.Motes.get(StateCnt);
      for (DimCnt = 0; DimCnt < NumDims; DimCnt++) {
        link = this.US.get(DimCnt);
        usnode = link.US;
        FireVal = usnode.Motes.get(StateCnt).Get_Outfire();
        FireVal = Globals.ActFun(FireVal);// Do this for raw space motes, not curve space motes.
        mote.loc[DimCnt] = FireVal;
        DimCnt++;
      }
      StateCnt++;
    }
  }
  /* ****************************************************** */
  @Override public double Mote_ActFun(double rawheight) {
//    return rawheight;// key difference
    return Globals.ActFun(rawheight);// key difference
  }
  /* ****************************************************** */
  @Override public void Pass_Back_Corrector() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.Apply_Height();
    }
  }
  /* ****************************************************** */
  public double GetSquishedPlaneHeight(PointNd pnt) {
    double PlaneHeight = this.planeform.Get_Plane_Height(pnt);
    return Globals.ActFun(PlaneHeight);
  }
  /* ****************************************************** */
  public void Walk_Test2(PointNd mote) {
    double Fineness = 0.2;
    double WalkerNadirSquishedHeight, PrevDistance, Distance, MoteDeltaHeight;
    PointNd Increment = new PointNd(this.planeform), Walker = new PointNd(mote);
    Increment.Set_Height(0.0);
    Increment.Unitize();
    Increment.Multiply(Fineness);
    /*
    so we can start walker at nadir of mote
    but we must know what direction to walk
    if mote is above nadir, walk toward uphill.
    if mote is below nadir, walk toward downhill. (yeah I know nadir means below, but whatever)
    uphill increment is same as plane slopes
    downhill increment is uphill times -1.0
    
    so walk from nadir until distance starts to increase
    then could invert Increment, Increment.Multiply(0.1) again, and walk backward until distance starts to increase again.
    reverse again, with finer steps again, until satisfied. 
     */
    PrevDistance = Double.MAX_VALUE;// ridiculous to lose first comparison
    WalkerNadirSquishedHeight = this.GetSquishedPlaneHeight(Walker);
    MoteDeltaHeight = mote.Get_Height() - WalkerNadirSquishedHeight;// if mote is above its nadir, then +, if below, then -. 
    Increment.Multiply(MoteDeltaHeight > 0.0 ? 1.0 : -1.0);
    Distance = Math.abs(MoteDeltaHeight);// first distance is straight down
    for (int cnt = 0; cnt < 8; cnt++) {
      do {// while getting closer
        PrevDistance = Distance;
        Walker.Add(Increment);
        WalkerNadirSquishedHeight = this.GetSquishedPlaneHeight(Walker);
        Walker.Set_Height(WalkerNadirSquishedHeight);
        Distance = mote.Get_Distance(Walker);
        System.out.println(Walker.To_String() + " " + Distance);
      } while (PrevDistance >= Distance);
      Increment.Multiply(-Fineness);// reverse direction and increase resolution
    }
    // at this point, PrevDistance will be the closest.  Prev Walker? 
    // correction vector will be Walker.Subtract(mote);
    System.out.println();
  }
  /* ****************************************************** */
  public void Walk_Test(PointNd mote) {
    Roto_Plane plane = this.planeform;
    PointNd Increment = new PointNd(plane), Walker = new PointNd(mote), MinPin = new PointNd(mote.ndims);
    Increment.Set_Height(0.0);
    Increment.Unitize();
//    Increment.Multiply(Math.sqrt(2.0));
    Increment.Multiply(0.1);
    double WalkerNadirHeight, NadirSquishedHeight, Distance, MinDistance;
    MinDistance = Double.MAX_VALUE;// ridiculous to lose first comparison

    for (int cnt = 0; cnt <= 20; cnt++) {
      WalkerNadirHeight = plane.Get_Plane_Height(Walker);
      NadirSquishedHeight = Globals.ActFun(WalkerNadirHeight);
      Walker.Set_Height(NadirSquishedHeight);
      Distance = Walker.Get_Distance(mote, mote.ndims);
      if (MinDistance > Distance) {
        MinDistance = Distance;
        MinPin.Copy_From(Walker);
      }
      System.out.println(Walker.To_String() + ", " + MinPin.To_String() + ", " + Distance);
//      System.out.println(WalkerNadirHeight + ", " + NadirSquishedHeight);
      Walker.Add(Increment);
    }
    System.out.println();
  }
  /* ****************************************************** */
  public void Calculate_Correctors(PointNd mote, PointNd Attraction) {// not functional yet. will create correctors to backpropagate
    // Will this be in raw linear space? 
    // No. At least for the corrs we pass back, they need to be in curve space.  Sure? 
    // Wait is mote pre-squished or not?

    /*
     So curve space correctors are done by:
     get point on linear plane this mote is normal to.
     get point on linear plane beneath this mote.
     squish both points through actfun to get curve space coords (just change heights).
     draw line between the two points.
     squishmote = this mote's post-curve location
     get normal from squishmote to the line between the squished points. 
     not perfect, but within bounds? 
    
     !!!
     ideal solution would be to get this mote's post-curve normal to post-curve plane. wtf kind of weird calculus for that? 
     treesearch for that would be insane. 
    
     or could get 2 points for exact slope right under the mote, then do normal to that.
    
     */
    {
      /*
       Now we want
       mote nadir on raw plane
       mote nadir squished
      
       mote closest point on raw plane
       mote closest point squished
      
       dupe.Z += (NadirSquished - NadirRaw); // now dupe nadir point is aligned with squished nadir
      
       // rebase dupe XYZ on mote nadir squished
       dupe.X -= mote.X; dupe.Y -= mote.Y; dupe.Z -= NadirSquished;// now dupe nadir is at 000
      
       // also rebase closest points (both are the same except for Z)
       ClosestSquished.X -= mote.X; ClosestSquished.Y-= mote.Y; ClosestSquished.Z -= NadirSquished;
       ClosestRaw.X -= mote.X; ClosestRaw.Y-= mote.Y; ClosestRaw.Z -= NadirSquished;
      
       // next we want ratios
       ratio = (ClosestSquished.Z / ClosestRaw.Z) ???
       dupe.Multiply(ratio);// only multiply XY by ratio, not Z ?  
      
       // then rebase dupe back to where it was
       dupe.X += mote.X; dupe.Y += mote.Y; dupe.Z += NadirSquished;// now dupe nadir is at 000
      
       // finally get attraction from squished mote to dupe
      
       */
    }

    double NadirSquishedHeight, ClosestSquishedHeight, MoteHeight, SquishedMoteHeight, MoteClearance;
    double ratio;

    Roto_Plane plane = this.planeform;
    int ndims = mote.ndims;
    int ninputs = mote.ninputs;

    Roto_Plane dupe = new Roto_Plane(plane);

    PointNd Closest = new PointNd(ndims), ClosestSquished = new PointNd(ndims);
    PointNd Nadir = new PointNd(ndims), NadirSquished = new PointNd(ndims);
    {// do we need this?
      PointNd SquishedMote = new PointNd(mote);
      SquishedMoteHeight = Globals.ActFun(SquishedMote.Get_Height());
      SquishedMote.Set_Height(SquishedMoteHeight);
    }

    {// kludgey. just want point on plane closest to mote.
      plane.Get_Attraction_To_Plane(mote, Closest);// attraction to FLAT plane
      Closest.Add(mote);// add mote to ClosestSquished to get closest point on flat plane to mote
      ClosestSquished.Copy_From(Closest);
      ClosestSquishedHeight = Globals.ActFun(ClosestSquished.Get_Height());
      ClosestSquished.Set_Height(ClosestSquishedHeight);// ClosestSquished is now squished
    }
    {
      Nadir.Copy_From(mote);
      Nadir.loc[ninputs] = plane.Get_Plane_Height(mote);
      NadirSquished.Copy_From(Nadir);
      NadirSquishedHeight = Globals.ActFun(Nadir.Get_Height());
      NadirSquished.Set_Height(NadirSquishedHeight);
    }
    {
      dupe.loc[ninputs] += (NadirSquished.Get_Height() - Nadir.Get_Height()); // now dupe nadir point is aligned with squished nadir

      // Rebase dupe XYZ on mote nadir squished.
      dupe.Subtract(NadirSquished);// now dupe mote nadir is at 000

      // also rebase closest points (both are the same except for Z)
      Closest.Subtract(NadirSquished);
      ClosestSquished.Subtract(NadirSquished);

      // next we want ratios
      ratio = (ClosestSquished.Get_Height() / Closest.Get_Height());
      dupe.Multiply(ratio, ninputs);// ???  only multiply XY by ratio, not Z ?  

      // Then rebase dupe back to where it was.
//       dupe.X += mote.X; dupe.Y += mote.Y; dupe.Z += NadirSquished;
      dupe.Add(NadirSquished);// now dupe mote nadir is at 000 origin

      // finally get attraction from squished mote to dupe
      dupe.Get_Attraction_To_Plane(mote, Attraction);// Generate the corrector 
    }

    PointNd pnt2d = new PointNd(2), motepnt2d = new PointNd(2);
    MoteHeight = mote.Get_Height();
    if (false) {
      NadirSquishedHeight = plane.Get_Plane_Height(mote);
      NadirSquishedHeight = Globals.ActFun(NadirSquishedHeight);// CurveHeightBeneath is now squished
      MoteClearance = MoteHeight - NadirSquishedHeight;
      motepnt2d.Assign(0.0, MoteClearance);
    }

    if (false) {
//    double RunX = Beneath.Get_Distance(SquishedNormal, ninputs);// get XY distance, ignore Z
      double RunX = mote.Get_Distance(ClosestSquished, ninputs);// get XY distance, ignore Z
      double RiseY = ClosestSquished.loc[ninputs] - NadirSquishedHeight;// vertical displacement
//    double Slope = RiseY / RunX; pnt2d.Assign(1.0, Slope);
//    pnt2d.Assign(-1.0 / Slope, 1.0);// pnt2d is the normal now
      pnt2d.Assign(-1.0 / RiseY, RunX);// pnt2d is the normal now
      pnt2d.Unitize();// unit vector normal to line
      double ProjLen = motepnt2d.Dot_Product(pnt2d);// mote height projected onto normal, makes length of normal.
      pnt2d.Multiply(ProjLen);// vector from mote to curved-plane-approximation in 2D.  now what? 
      /*
       then maybe make the XY length of ClosestSquished and make it equal to X value of pnt2d,
       and make the Z length of ClosestSquished equal to Z value of pnt2d? 
    
       that would mean 
       unitize ClosestSquished X and Y, then multiply them by X len of pnt2d
       then set ClosestSquished Z value = pnt2d. Z height?
    
    
       */
 /*
       Then here, search between FlatHit and Beneath for closest point on curve to mote
    
       Crude but faster approach
       Normal to RunX and RiseY is -RiseY(as X) and RunX(as Y)
       Normal to Slope is -1/Slope
       But we also need the length from mote.
       project (mote height above curve) onto normal?
       length is sqrt(1 + (-1/Slope)^2)
       unit vector is x=1/length, y=Slope/length
       project mote onto pnt2d
       so we want mote height diff * unit vector height, (mote X diff is 0)
     
    
       More accurate but slower approaches
       one way is to just iterate from FlatHit to Beneath 
    
       or use calculus (would be best to find closest point to curve)
       YHeight = xin / Math.sqrt(1.0 + xin * xin);
       deltaX = moteX-xin, deltaY = moteY-YHeight;
       distance = sqrt( (deltaX*deltaX) + (deltaY*deltaY) );
       we want to find minimum distance. 
    
       */
    }
    // distant point XY but not Z are still along the normal. 
    // can we get normal in 2D with pythag line length as X, mote as 0,0, then use XY distance of result from mote
    // as new distance for flatspace normal? 
    // one catch is that target line can cut through the hill of the curve, or fall short. 
//    PointNd pdesire = new PointNd(ndims);
//    plane.Get_Attraction_To_Curve(mote, pdesire);// Generate the corrector 
//    for (int pcnt = 0; pcnt < pdesire.ninputs; pcnt++) {
//    }
  }
  /* ****************************************************** */
  @Override public void Ping() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      this.planeform.Train_Inlinks2(cpnt, Things.lrate);
    }
  }
  /* ****************************************************** */
  @Override public NodeTest1 Clone_Me() {
    NodeTest1 child = new NodeTest1();
    return child;
  }
}
