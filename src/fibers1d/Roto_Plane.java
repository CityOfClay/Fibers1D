package fibers1d;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Roto_Plane extends PlaneNd implements Things.Drawable {
  // the purpose of this class is to represent a sigmoid plane, to fit it to points, and to fit points to it.
  private PointNd pingvec;
  PointNd normal = null;
  PointNd desire = null;
  // These values below should come from NodeBox context!
  int xorg, yorg;
  /* ****************************************************** */
  public Roto_Plane(int num_dims) {
    super(num_dims);
    pingvec = new PointNd(ndims);
    normal = new PointNd(ndims);
    desire = new PointNd(ndims);
    this.Randomize(-0.1, 0.1);
  }
  /* ****************************************************** */
  public Roto_Plane(Roto_Plane donor) {
    super(donor.ndims);
    pingvec = new PointNd(ndims);
    normal = new PointNd(ndims);
    desire = new PointNd(ndims);
    this.Copy_From(donor);
  }
  /* ****************************************************** */
  public void Ping(PointNd pnt) {// attract plane to mote
    double phgt = pnt.loc[ninputs];
    // First get the height of the plane at this loc.
    double planehgt, curvehgt;
    planehgt = this.Get_Plane_Height(pnt);// get height of plane at location of this point
    curvehgt = Globals.ActFun(planehgt);// sigmoidify it
    // corrector is the distance from the sigmoid plane TOWARD this point's height
    double corr = phgt - curvehgt;
    // now create the recognition vector, based on this pnt's position, and 1.0 * this plane's height offset dimension
    pingvec.Copy_From(pnt);
//    for (int cnt = 0; cnt < ninputs; cnt++) {
//      pingvec.loc[cnt] = pnt.loc[cnt];
//    }
//    pingvec.loc[ninputs] = pnt.loc[ninputs];// 1.0;
    if (true) {
      pingvec.loc[ninputs] = planehgt + corr;
//      pingvec.loc[ninputs] = Reverse_ActFun(pingvec.loc[ninputs]);
      Train_Inlinks2(pingvec, Things.lrate);
    } else {
      Train_Inlinks(pingvec, ndims, Things.lrate, corr);
    }
  }
  /* ****************************************************** */
  public void Get_Attraction_To_Curve(PointNd pnt, PointNd Attraction) {// attract mote to curved plane
    double nadir_hgt = this.Get_Plane_Height(pnt);// height on raw plane at this point's position.
    double curved_plane_hgt = Globals.ActFun(nadir_hgt);// height on curved plane at this point's position.
    double delta_hgt = curved_plane_hgt - pnt.loc[ninputs];// distance from this point toward sigmoid plane.
    PointNd normal = new PointNd(ndims);
    this.Plane_Ramp_To_Normal(normal);// get normal to raw plane.
    Attraction.Clear();// use Attraction as temporary vector to project delta onto normal
    Attraction.loc[ninputs] = delta_hgt;
    double corrlen = Attraction.Dot_Product(normal);// project delta onto normal, to get straight distance to plane.
    normal.Multiply(corrlen);// multiply unit normal by corrector length to get correction vector.
    Attraction.Copy_From(normal);
  }
  /* ****************************************************** */
  public void Get_Attraction_To_Plane(PointNd pnt, PointNd Attraction) {// attract mote to flat plane
    double nadir_hgt = this.Get_Plane_Height(pnt);// height on raw plane at this point's position.
    double delta_hgt = nadir_hgt - pnt.loc[ninputs];// distance from this point toward flat plane.
    PointNd normal = new PointNd(ndims);
    this.Plane_Ramp_To_Normal(normal);// get normal to raw plane.
    Attraction.Clear();// use Attraction as temporary vector to project delta onto normal
    Attraction.loc[ninputs] = delta_hgt;
    double corrlen = Attraction.Dot_Product(normal);// project delta onto normal, to get straight distance to plane.
    normal.Multiply(corrlen);// multiply unit normal by corrector length to get correction vector.
    Attraction.Copy_From(normal);
  }
  /* *************************************************************************************************** */
//    public void Train_Inlinks(PointNd invec, int ninputs_local, double lrate, double corrector) {// from FlatNetFork
//      double invec_squared = invec.Magnitude_Squared(ninputs_local);
//      if (invec_squared == 0.0) {
//        invec_squared = fudge;
//      }
//      for (int cnt = 0; cnt < ninputs_local; cnt++) {
//        double adj = (invec.loc[cnt] / invec_squared);// unitary adjustment tool
//        adj = adj * corrector * lrate;
//        this.loc[cnt] += adj;
//      }
//    }/* Train_Inlinks */
  /* ****************************************************** */
  public void Train_Inlinks(PointNd invec, int ninputs_local, double lrate, double corrector) {
    double invec_squared = invec.Magnitude_Squared(ninputs_local);
    if (invec_squared == 0.0) {
      invec_squared = Logic.fudge;
    }
//    invec_squared = 1.0;// disable
    for (int cnt = 0; cnt < ninputs_local; cnt++) {
      double adj = (invec.loc[cnt] / invec_squared);// unitary adjustment tool
      adj = adj * corrector * lrate;
//      adj = adj * lrate;
      this.loc[cnt] += adj;
    }
  }
  /*
   for a line:
   ping x, y
   to cross at xy
   slope = y/x
  
   then get plane height at x
   subtract that from input y
   slopecorr = diff/x 
   plane slope at x += slopecorr*lrate;
  
   zdiff = input y - planeheight at x
   plane z const += zdiff * lrate;
   */
 /* ****************************************************** */
  public void Train_Inlinks2(PointNd PingMote, double lrate) {
    // Caveat: this only trains for raw plane. must de-sigmoid PingPnt first to work right. 
    double Diff_Height, Slope_Corr, Run;
    double NinputsFloat = this.ninputs;// Each factor can only have a fraction of effect or else we will overshoot.
    double Plane_Height = this.Get_Plane_Height(PingMote);
    double Goal_Height = PingMote.loc[ninputs];// final dimension is height of mote 
    Diff_Height = Goal_Height - Plane_Height;
    for (int cnt = 0; cnt < ninputs; cnt++) {
      Run = PingMote.loc[cnt];
      Slope_Corr = Diff_Height / Run;
      this.loc[cnt] += (Slope_Corr / NinputsFloat) * lrate;
    }
    this.loc[ninputs] += (Diff_Height / NinputsFloat) * lrate;
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Graphics2D gr = dc.gr;
    Things.TransForm mytrans = new Things.TransForm();
    mytrans.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);
    Plot_Gradient(dc);
  }
  public void Plot_Gradient(Things.DrawingContext dc) {
    /* all about the gradient for display */
    Things.TransForm tr = dc.transform;
    Graphics2D g2 = dc.gr;
    this.Plane_Ramp_To_Normal(normal);
    double hgt = this.loc[0];
    double grad_x0;
    double grad_y0;
    double grad_x1, grad_y1;
    PointNd steepest = new PointNd(ndims);
    normal.Get_Steepest(steepest);
    if (normal.loc.length > 1) {
      boolean nop = true;// breakpoint
    } else {
      boolean nop = true;// breakpoint
    }
    double[] ratios = new double[ninputs];// the ratios are NOT the inverse slopes.  they are from the steepest line.
    for (int cnt = 0; cnt < ninputs; cnt++) {
      ratios[cnt] = steepest.loc[cnt] / steepest.loc[ninputs];// inverse slope for each shadow of the steepest
    }
    if (true) {
      double offset = this.loc[ninputs];
      double height0 = -1.0 - offset;
      double height1 = 1.0 - offset;
      double brad = tr.xscale;// Bounds.Rad(ninputs);
      //gradx0 = (int) (brad * (height0 * ratios[0])); grad_y0 = (int) (brad * (height0 * ratios[1])); grad_x1 = (int) (brad * (height1 * ratios[0])); grad_y1 = (int) (brad * (height1 * ratios[1]));
      height0 *= brad;
      height1 *= brad;
      grad_x0 = (int) ((height0 * ratios[0]));
      grad_y0 = (int) ((height0 * ratios[1]));
      grad_x1 = (int) ((height1 * ratios[0]));
      grad_y1 = (int) ((height1 * ratios[1]));
    }
    Color StartColor = new Color(0.0f, 0.0f, 1.0f);//Color startColor = Color.blue;
    Color EndColor = new Color(1.0f, 0.0f, 0.0f);//Color endColor = Color.red;

    GradientPaint gradient;
//        gradient = new GradientPaint((float) (tr.xoffs + grad_x0), (float) (tr.yoffs + grad_y0), StartColor, (float) (tr.xoffs + grad_x1), (float) (tr.yoffs + grad_y1), EndColor);// A non-cyclic gradient
    Point2D.Double StartPnt, EndPnt;
    StartPnt = new Point2D.Double((tr.xoffs + grad_x0), (tr.yoffs + grad_y0));
    EndPnt = new Point2D.Double((tr.xoffs + grad_x1), (tr.yoffs + grad_y1));

    if (StartPnt.distance(EndPnt) == 0.0) {
      g2.setPaint(Color.green);
    } else {
      gradient = new GradientPaint(StartPnt, StartColor, EndPnt, EndColor);
      g2.setPaint(gradient);
    }

    if (true) {
      int rxorg = (int) (tr.xoffs - tr.xscale);
      int ryorg = (int) (tr.yoffs - tr.yscale);
      int rwdt = (int) (2.0 * tr.xscale);
      int rhgt = (int) (2.0 * tr.yscale);
      g2.fillRect(rxorg, ryorg, rwdt, rhgt);
      g2.setColor(Color.white);
      g2.drawLine((int) (tr.xoffs + grad_x0), (int) (tr.yoffs + grad_y0), (int) (tr.xoffs + grad_x1), (int) (tr.yoffs + grad_y1));
    }
  }
}
