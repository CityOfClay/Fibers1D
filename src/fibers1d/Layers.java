package fibers1d;

import static fibers1d.Things.BoxSpacing;
import static fibers1d.Things.ColumHeight;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Layers implements Things.Drawable, Things.Causal {
  public ArrayList<Network> Network_List;
  public double xorg, yorg;
  Network net_last;
  public Layers() {
    Network_List = new ArrayList<Network>();
    xorg = 50;
    yorg = 50;
  }
  public void Make_Layers(int num_layers, int Layer_Size) {
    int last_layer = num_layers - 1;
    NodeIn factory = new NodeIn();// to do: move to this pattern

    int lcnt = 0;
    {
      Network net = new Network();
      net.Make_Layer(factory, Layer_Size);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);
      lcnt++;
    }
    while (lcnt < last_layer) {
      Network net = new Network();
      net.Make_Layer(Layer_Size);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);
      lcnt++;
    }
    {
      NodeOut factoryout = new NodeOut();// to do: move to this pattern
      Network net = new Network();
      net.Make_Layer(factoryout, Layer_Size);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);
      lcnt++;
    }

    Network net_prev = this.Network_List.get(0);
    for (lcnt = 1; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
//        net.Connect_From_Other(net_prev);
      net.Connect_Weave(net_prev);
      net_prev = net;
//        net.Init_Unique();
      net.Init_Random_Unique();
    }

    Network net_first = this.Network_List.get(0);
    net_first.Init_Unique();
    net_first.Set_Explodable(false);// just to set NoExplode

    // In the last layer, we want to set the desired output heights
    this.net_last = this.Network_List.get(this.Network_List.size() - 1);
    this.net_last.Init_Xor();
//    this.net_last.Init_And();
//      this.net_last.Init_Or();
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);

    int num_layers = Network_List.size();
    for (int lcnt = 0; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      net.Draw_Me(mydc);
    }
  }
  @Override public void Collect_And_Fire() {
    int num_layers = Network_List.size();
    for (int lcnt = 0; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      net.Collect_And_Fire();
    }
  }
  @Override public void Pass_Back_Corrector() {
    int num_layers = Network_List.size();

    int last_layer = num_layers - 1;
    for (int lcnt = last_layer; lcnt >= 0; lcnt--) {
      Network net = this.Network_List.get(lcnt);
      if (lcnt == 0) {
        System.out.print("");// breakpoint
      }
      net.Pass_Back_Corrector();
    }
//      if (false) {
//        for (int lcnt = 0; lcnt < num_layers; lcnt++) {
//          Network net = this.Network_List.get(lcnt);
//          net.Pass_Back_Corrector();
//        }
//      }
  }
  public void Apply_Corrector() {
    int num_layers = Network_List.size();
    int last_layer = num_layers - 1;
    for (int lcnt = last_layer; lcnt >= 0; lcnt--) {
      Network net = this.Network_List.get(lcnt);
      net.Apply_Corrector();
    }
  }
  public void RunCycle() {
    this.Collect_And_Fire();
    this.Pass_Back_Corrector();
    this.Apply_Corrector();
    double Nonlinearity = this.Get_Avg_Nonlinearity();
    System.out.println("Nonlinearity:" + Nonlinearity);
//    this.net_last.Init_And();
//    this.net_last.Init_Xor();
    /* collect and fire to nowfire (all)
     * for all {
     *   calc desire 
     *   pass back desire (to links)
     * }
     * for all {
     *   collect desire from links
     *   apply desire to my prevfire
     *   push nowfire to links, set prevfire to nowfire.
     * }
     * 
     * each node only has a nowfire.  prev fire is stored in links.
     * 
     */
  }
  public double Get_Avg_Nonlinearity() {
    int num_layers = Network_List.size();
    int start_layer = 0;// first few layers should be getting *more* nonlinear
    double Sum = 0.0;
    for (int lcnt = start_layer; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      Sum += net.Get_Avg_Nonlinearity();
    }
    return Sum / (double) (num_layers - start_layer);
  }
}
