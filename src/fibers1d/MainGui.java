package fibers1d;

import fibers1d.Things.DrawingContext;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author MultiTool
 */
public class MainGui {
  JFrame frame;
  DrawingPanel drawpanel;
  Layers layers = null;
  boolean SingleTest = true;
  NodeTest nodetest = new NodeTest();
  NodeTest1 nodetest1 = new NodeTest1();
  /* ********************************************************************************* */
  public MainGui() {
    if (true) {
      NodeTest1 nt1 = new NodeTest1();
//      nt1.planeform.Assign(0.001, 0.001, 0.0);
//      nt1.planeform.Assign(1.0, 1.0, 0.0);
      nt1.planeform.Assign(2.0, 2.0, 0.0);
//      nt1.planeform.Assign(2.0, 0.5, 0.0);
      Mote mote = new Mote(nt1, nt1.Num_Dims);
      PointNd Attraction = new PointNd(nt1.Num_Dims);
      
//      mote.Assign(-1.0, -1.0, 0.0);
      mote.Assign(0.0, 0.0, 1.0);
//      nt1.Walk_Test(mote);
      nt1.Walk_Test2(mote);

      for (double cnt = -1.0; cnt < 1.0; cnt += 0.1) {
//        mote.Assign(cnt, cnt, -1.0);
//        mote.Assign(cnt, cnt, 0.0);
        mote.Assign(cnt, cnt, 1.0);
        nt1.Calculate_Correctors(mote, Attraction);
        Attraction.Print_Me();
      }

      System.out.println();
    }
    if (false) {
      double xin;
      double out;
      for (xin = -0.999; xin < 0.999; xin += 0.001) {
        out = Globals.Reverse_ActFun(xin);// test: yes this works
        out = Globals.ActFun(out);
        System.out.println(out);
      }
    }
    if (SingleTest) {
//      nodetest.Test();
      nodetest1.Test();
    } else {
      int Layer_Size = 2;
      layers = new Layers();
      layers.Make_Layers(2, Layer_Size);
//    layers.Make_Layers(3, Layer_Size);
//    layers.Make_Layers(4, Layer_Size);
//    layers.Make_Layers(8, Layer_Size);
//    layers.Make_Layers(12, Layer_Size);
//    layers.Make_Layers(16, Layer_Size);
//    layers.Make_Layers(32, Layer_Size);
//    layers.Make_Layers(64, Layer_Size);
//    layers.Make_Layers(128, Layer_Size);
//    layers.Make_Layers(256, Layer_Size);
    }
  }
  /* ********************************************************************************* */
  public void Init() {
    this.frame = new JFrame();
    this.frame.setTitle("Fibers1D");
//    this.frame.setSize(700, 400);
    this.frame.setSize(1000, 900);
    this.frame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    Container contentPane = this.frame.getContentPane();
    this.drawpanel = new DrawingPanel();
    contentPane.add(this.drawpanel);
    this.drawpanel.BigApp = this;
    frame.setVisible(true);
  }
  /* ********************************************************************************* */
  public static class DrawingPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
    MainGui BigApp;
    int ScreenMouseX = 0, ScreenMouseY = 0;
    double MouseOffsetX = 0, MouseOffsetY = 0;

    /* ********************************************************************************* */
    public DrawingPanel() {
      this.Init();
    }
    /* ********************************************************************************* */
    public final void Init() {
      this.addMouseListener(this);
      this.addMouseMotionListener(this);
      this.addMouseWheelListener(this);
      this.addKeyListener(this);
    }
    /* ********************************************************************************* */
    public void Draw_Me(Graphics2D g2d) {
      // to do: create paramblob for drawing context, use it in things
      g2d.setBackground(Color.yellow);
      g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
      Things.TransForm tr = new Things.TransForm();

//      DrawingContext dc = new DrawingContext(); dc.gr = g2d;
      int wdt, hgt;

      wdt = this.getWidth();
      hgt = this.getHeight();

      Rectangle2D rect = new Rectangle2D.Float();
      rect.setRect(0, 0, wdt, hgt);

      Stroke oldStroke = g2d.getStroke();
      BasicStroke bs = new BasicStroke(5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
      g2d.setStroke(bs);
      g2d.setColor(Color.green);
      g2d.draw(rect);// green rectangle confidence check for clipping
      g2d.setStroke(oldStroke);
      DrawingContext dc = new DrawingContext();
      dc.gr = g2d;
      dc.transform = tr;
      if (this.BigApp.SingleTest) {
        Things.DrawingContext mydc = new Things.DrawingContext();
        mydc.gr = dc.gr;
        mydc.transform.Accumulate(dc.transform, 0, 0, 4.0, 4.0);
//        BigApp.nodetest.Draw_Me(mydc);
        BigApp.nodetest1.Draw_Me(mydc);
      } else {
        BigApp.layers.Draw_Me(dc);
        BigApp.layers.RunCycle();
      }
    }
    /* ********************************************************************************* */
    @Override public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      Draw_Me(g2d);// redrawing everything is overkill for every little change or move. to do: optimize this
      this.repaint();
//      try {
//        Thread.sleep(100);
//      } catch (Exception ex) {
//      }
    }
    /* ********************************************************************************* */
    @Override public void mouseDragged(MouseEvent me) {
    }
    @Override public void mouseMoved(MouseEvent me) {
      this.ScreenMouseX = me.getX();
      this.ScreenMouseY = me.getY();
    }
    /* ********************************************************************************* */
    @Override public void mouseClicked(MouseEvent me) {
    }
    @Override public void mousePressed(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseReleased(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseEntered(MouseEvent me) {
    }
    @Override public void mouseExited(MouseEvent me) {
    }
    /* ********************************************************************************* */
    @Override public void mouseWheelMoved(MouseWheelEvent mwe) {
      double XCtr, YCtr, Rescale;
      XCtr = mwe.getX();
      YCtr = mwe.getY();
      double finerotation = mwe.getPreciseWheelRotation();
      this.repaint();
    }
    /* ********************************************************************************* */
    @Override public void componentResized(ComponentEvent ce) {
    }
    @Override public void componentMoved(ComponentEvent ce) {
    }
    @Override public void componentShown(ComponentEvent ce) {
    }
    @Override public void componentHidden(ComponentEvent ce) {
    }
    /* ********************************************************************************* */
    @Override public void keyTyped(KeyEvent ke) {
    }
    @Override public void keyPressed(KeyEvent ke) {
    }
    @Override public void keyReleased(KeyEvent ke) {
    }
  }

}
