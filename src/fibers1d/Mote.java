package fibers1d;

import static fibers1d.Things.lrate;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Mote extends PointNd implements Things.Drawable, Things.Causal {// Control Point
  int Num_Upstreamers = 0;
  int Num_Downstreamers = 0;
  //public Mote[] US, DS;
  public Link_Mote[] US, DS;
  public double OutFire;
  public double Corrector;
  PointNd screenloc = new PointNd(2);// temporary but often-reused
  PointNd attractor;
  double radius, diameter;
  public NodeBox Parent;
  /* ****************************************************** */
  public static class Link_Mote {// link only to other Motes
    private Mote US, DS;
    public double Corrector;// FireVal, 
    public Link_Mote() {
    }
    public Link_Mote(Mote US0, Mote DS0) {
      US = US0;
      DS = DS0;
    }
    public double Get_Outfire() {
      //return this.FireVal;
      return this.US.Get_Outfire();
    }
  }
  /* ****************************************************** */
  public Mote(NodeBox NewParent, int num_dims) {
    super(num_dims);
    attractor = new PointNd(num_dims);
    this.Parent = NewParent;
    for (int cnt = 0; cnt < num_dims; cnt++) {
      this.loc[cnt] = 0.0;
    }
    Num_Upstreamers = 0;
    //US = new Mote[this.ninputs]; DS = new Mote[2];
    US = new Link_Mote[this.ninputs];
    DS = new Link_Mote[2];
    radius = 2.0;
    diameter = radius * 2.0;
    OutFire = 0;
  }
  /* ****************************************************** */
  public double Get_Height() {
    return this.loc[this.ninputs];
  }
  /* ****************************************************** */
  public void Set_Height(double Height) {
    this.loc[this.ninputs] = Height;
  }
  public double Get_Outfire() {
    return this.loc[this.ninputs];
  }
  public double Get_Mapped_Outfire() {
    return this.Parent.Mote_ActFun(this.Get_Outfire());
  }
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.TransForm tr = dc.transform;
    Graphics2D gr = dc.gr;
    tr.To_Screen(this.loc[0], this.loc[1], screenloc);

    double height = this.loc[ninputs];
    height = this.Parent.Mote_ActFun(height);// parent can represent as raw or curved

    float pntband = (float) ((height + 1.0) / 2.0);// map to range 0.0 - 1.0
    float red = pntband, blue = (float) 1.0 - pntband;
    blue = blue < 0 ? 0 : (blue > 1 ? 1 : blue);
    red = red < 0 ? 0 : (red > 1 ? 1 : red);
    Color pnt_color = new Color(red, 0.0f, blue);

    gr.setColor(pnt_color); // need to add code to xmap color here
    gr.fillRect((int) (screenloc.loc[0] - radius), (int) (screenloc.loc[1] - radius), (int) diameter, (int) diameter);

    gr.setColor(Color.black);
    gr.drawRect((int) (screenloc.loc[0] - radius), (int) (screenloc.loc[1] - radius), (int) diameter, (int) diameter);

    /*
     * {
     * if (true) { double factor = 100.0; gr.setColor(Color.magenta); * draw
     * the movement line * gr.drawLine( (int) (xorg + Bounds.Rad(0) *
     * pnt.loc[0]), (int) (yorg + Bounds.Rad(1) * pnt.loc[1]), (int) (xorg +
     * Bounds.Rad(0) * (pnt.loc[0] + pnt.delta[0] * factor)), (int) (yorg +
     * Bounds.Rad(1) * (pnt.loc[1] + pnt.delta[1] * factor))); } }
     */
  }
  @Override public void Collect_And_Fire() {// Just moves me to the XY location of my input fires
    Corrector = 0.0;
    double SumFire = 0.0;
    for (int pcnt = 0; pcnt < Num_Upstreamers; pcnt++) {
      double infire = this.US[pcnt].Get_Outfire();
      /*
       * for my attraction point, make a vector of all the upstreamers outfire
       * values.
       */
      infire = this.Parent.Mote_ActFun(infire);
      attractor.loc[pcnt] = infire;
      /*
       * for sum outfire, mult each inlinks fire value by the tilt of our
       * plane in that dimension. then add them.
       *
       */
      // more to go here
    }
    OutFire = this.loc[ninputs];
    for (int cnt = 0; cnt < Num_Upstreamers; cnt++) {
      this.loc[cnt] = attractor.loc[cnt];
    }
  }
  @Override public void Pass_Back_Corrector() {
    Roto_Plane plane = this.Parent.planeform;
    // First generate the corrector 
    PointNd pdesire = new PointNd(this.ndims);
    plane.Get_Attraction_To_Curve(this, pdesire);
    if (true) {//this.Parent.Num_Us > 0) {// hack
      for (int pcnt = 0; pcnt < this.ninputs; pcnt++) {
        Link_Mote lnk = this.US[pcnt];
        lnk.Corrector = pdesire.loc[pcnt];
      }
    } else {
      boolean nop = true;// breakpoint
    }
    if (false) {// node can ask itself to become more linear too
      double lrate2 = 1.0;//0.2;//vfactor
      this.loc[ninputs] += pdesire.loc[this.ninputs] * lrate * lrate2;
      if (Double.isNaN(this.loc[ninputs])) {
        boolean nop = true;// breakpoint  
      }
//        double vfactor = 0.0;//0.99;//0.0;// 0.09 * 0.3;// works better with 7 layers, and maybe 3
//        pdesire.loc[ninputs] *= vfactor;
//        pnt.loc[ninputs] += pdesire.loc[ninputs];
    }
  }
  void Apply_Height() {
    Roto_Plane plane = this.Parent.planeform;
    /* First generate the corrector */
    PointNd pdesire = new PointNd(this.ndims);
    plane.Get_Attraction_To_Curve(this, pdesire);
    if (true) {// node can ask itself to become more linear too
      double lrate2 = 1.0;//0.2;//vfactor
      this.loc[ninputs] += pdesire.loc[this.ninputs] * lrate * lrate2;
    }
  }
  public void Gather_Correctors() {
    Corrector = 0.0;
    for (int pcnt = 0; pcnt < this.Num_Downstreamers; pcnt++) {
      Corrector += this.DS[pcnt].Corrector;
    }
    Apply_Corrector();
  }
  public void Apply_Corrector() {
    if (this.Num_Downstreamers > 0) {// hacky hack
      Corrector *= Globals.sigmoid_deriv(this.loc[ninputs]);
    }
    // this is all wrong.  
    this.loc[ninputs] += Corrector;
  }
  public void ConnectIn(Mote other) {
    Link_Mote lnk = new Link_Mote(other, this);
    US[this.Num_Upstreamers] = lnk;
    this.Num_Upstreamers++;
    other.DS[other.Num_Downstreamers] = lnk;
    other.Num_Downstreamers++;
  }
}
