package fibers1d;

import static fibers1d.Things.ndims_init;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeBox implements Things.Drawable, Things.Causal {
  public Mote_List Motes;
  public int Num_Us, Num_Ds;
  public ArrayList<Axon> US, DS;
  public Roto_Plane planeform;
  public double xorg, yorg, xscale, yscale;
  public double FitError, FitErrorFlywheel = 0.0;
  /* ****************************************************** */
  public static class Axon {// link to other Nodes
    public NodeBox US, DS;
    public double Corrector;// FireVal, 
    public Axon() {
    }
    public Axon(NodeBox US0, NodeBox DS0) {
      US = US0;
      DS = DS0;
      US0.DS.add(this);
      DS0.US.add(this);
    }
    public double Get_Outfire() {
      return this.US.Get_Outfire();
    }
  }
  /* ****************************************************** */
  public static class Mote_List extends ArrayList<Mote> {
    public NodeBox Parent;
    public Mote_List(NodeBox NewParent, int newsize) {
      this.Parent = NewParent;
      PointNd avg = new PointNd(ndims_init);
      for (int cnt = 0; cnt < newsize; cnt++) {
        Mote pnt = new Mote(this.Parent, ndims_init);
        pnt.Randomize(-1.0, 1.0);
        this.add(pnt);
      }
      avg.Multiply(newsize);// experiment to move all the points to the centroid
    }
    /* ****************************************************** */
    public void Get_Average(PointNd ret_avg) {
      ret_avg.Clear();
      for (PointNd pnt : this) {
        ret_avg.Add(pnt);
      }
      ret_avg.Multiply(1.0 / (double) this.size());
    }
    /* ****************************************************** */
    public void CheckNAN() {
      for (PointNd pnt : this) {
        pnt.CheckNAN();
      }
    }
  }/* Mote_List */
 /* ****************************************************** */

  public NodeBox() {
    xscale = yscale = 12.0;
    Num_Us = Num_Ds = 0;
    this.US = new ArrayList<Axon>();
    this.DS = new ArrayList<Axon>();
    Motes = new Mote_List(this, 0);
    planeform = new Roto_Plane(3);
  }
  public double Get_Outfire() {
    return -Double.MAX_VALUE;// nonsense placeholder
//    return this.US.Get_Outfire();
  }
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, this.xscale, this.yscale);

    PointNd boxmin = new PointNd(2);
    PointNd boxmax = new PointNd(2);
    double xmin = -1.0, ymin = -1.0, xmax = 1.0, ymax = 1.0;
    mydc.transform.To_Screen(xmin, ymin, boxmin);
    mydc.transform.To_Screen(xmax, ymax, boxmax);

    dc.gr.setColor(Color.green);
    dc.gr.drawRect((int) (boxmin.loc[0]), (int) (boxmin.loc[1]), (int) (boxmax.loc[0] - boxmin.loc[0]), (int) (boxmax.loc[1] - boxmin.loc[1]));

    this.planeform.Draw_Me(mydc);

    {
      PointNd Zero = new PointNd(2);
      mydc.transform.To_Screen(0, 0, Zero);
      PointNd ErrPnt = new PointNd(2);
      mydc.transform.To_Screen(xmin, this.FitError, ErrPnt);
      dc.gr.setColor(Color.black);
      dc.gr.fillRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
      dc.gr.setColor(Color.white);
      dc.gr.drawRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
    }

    for (Mote mote : Motes) {
      mote.Draw_Me(mydc);
    }
  }
  public void Init_States(int num_states) {
    Mote cpnt;
    double amp = 1.0;
    double arbitrary_output = -0.5;
    for (int pcnt = 0; pcnt < num_states; pcnt++) {
      cpnt = new Mote(this, 3);
      cpnt.loc[0] = ((pcnt & 1) - 0.5) * amp;
      cpnt.loc[1] = (((pcnt >> 1) & 1) - 0.5) * amp;
      //arbitrary_output = (Logic.wheel.nextDouble() * 2.0) - 1.0;
      cpnt.loc[2] = arbitrary_output;//cpnt.loc[0] + cpnt.loc[1];
      //cpnt.Randomize(-1.0, 1.0);
      Motes.add(cpnt);
      arbitrary_output += 0.333;
    }
  }
  public boolean Explodable = true;
  public void Set_Explodable(boolean Exp) {
    this.Explodable = Exp;
  }
  public void Init_Random_Unique() {// hacky
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    Mote cpnt;
    double level = 0.0, minjump = 0.1, range = 0.1;
    if (false) {// random shuffled
      double[] shuffle = new double[num_pnts];
      for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
        level += minjump + (Logic.wheel.nextDouble() * range);
        shuffle[pcnt] = (((double) level) - 0.5) * amp;
      }
      double temp;
      int randex;
      for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
        randex = Logic.wheel.nextInt(num_pnts);
        temp = shuffle[pcnt];
        shuffle[pcnt] = shuffle[randex];
        shuffle[randex] = temp;
      }
      for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
        cpnt = this.Motes.get(pcnt);
        cpnt.loc[cpnt.ninputs] = shuffle[pcnt];
        System.out.println(cpnt.loc[cpnt.ninputs]);
      }
    } else {
      for (int pcnt = 0; pcnt < num_pnts; pcnt++) {// random sorted
        cpnt = this.Motes.get(pcnt);
        level += minjump + (Logic.wheel.nextDouble() * range);
        cpnt.loc[cpnt.ninputs] = (((double) level) - 0.5) * amp;
      }
      if (true) {// random shuffled
        double temp;
        int randex;
        Mote cpnt0;
        Mote cpnt1;
        for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
          randex = Logic.wheel.nextInt(num_pnts);
          cpnt0 = this.Motes.get(pcnt);
          cpnt1 = this.Motes.get(randex);
          temp = cpnt0.loc[cpnt0.ninputs];
          cpnt0.loc[cpnt0.ninputs] = cpnt1.loc[cpnt1.ninputs];
          cpnt1.loc[cpnt1.ninputs] = temp;
        }
      }
    }
  }
  public void Init_Unique() {
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    int last_pnt = num_pnts - 1;
    Mote cpnt;
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      cpnt = this.Motes.get(pcnt);
      double val = ((double) pcnt) / (double) last_pnt;
      cpnt.loc[cpnt.ninputs] = (((double) val) - 0.5) * amp;
      System.out.println(cpnt.loc[cpnt.ninputs]);
    }
  }
  public void Init_Same(double Value) {// nonsense value, only for testing
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.loc[cpnt.ninputs] = Value;
    }
  }
  public void Init_Xor() {
    this.Explodable = false;
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int xorval = (pcnt & 1) ^ ((pcnt >> 1) & 1);
      cpnt.loc[cpnt.ninputs] = (((double) xorval) - 0.5) * amp;
    }
  }
  public void Init_And() {
    this.Explodable = false;
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int andval = (pcnt & 1) & ((pcnt >> 1) & 1);
      cpnt.loc[cpnt.ninputs] = (((double) andval) - 0.5) * amp;
    }
  }
  public void Init_Or() {
    this.Explodable = false;
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int andval = (pcnt & 1) | ((pcnt >> 1) & 1);
      cpnt.loc[cpnt.ninputs] = (((double) andval) - 0.5) * amp;
    }
  }
  public void ConnectIn(NodeBox upstreamer) {
    this.ConnectInAxon(upstreamer);
    this.ConnectInMotes(upstreamer);
    this.Num_Us++;
    upstreamer.Num_Ds++;
  }
  public void ConnectInAxon(NodeBox upstreamer) {
    Axon axon = new Axon(upstreamer, this);
//      upstreamer.DS.add(axon); this.US.add(axon);
  }
  public void ConnectInMotes(NodeBox upstreamer) {// in the long run motes will no longer connect directly to each other
    int Num_Motes = upstreamer.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote us_cpnt = upstreamer.Motes.get(pcnt);
      Mote my_cpnt = this.Motes.get(pcnt);
      my_cpnt.ConnectIn(us_cpnt);
    }
  }
  @Override public void Collect_And_Fire() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote mote = this.Motes.get(pcnt);
      mote.Collect_And_Fire();
    }
    /*
    Redo this:
    my mote (state) is the major loop,
    upstream node is the minor loop.
    
    but first, we need links from node to node, and NO links from mote to mote.
    int statenum = 0;
    int dim = 0;
    double fireval;
    for (Mote mote : this.Motes){// this is my mote[statenum]
      dim = 0;
      for (Link_Node link : this.US){
        NodeBox usnode = link.US;
        fireval = usnode.Motes[statenum].Get_Fire();
        // fireval = ActFun(fireval);// Do this for raw space motes, not curve space motes.
        mote.loc[dim] = fireval;
        dim++;
      }
      statenum++;
    }
    
     */
  }
  /* ****************************************************** */
  public double Mote_ActFun(double rawheight) {// virtual
    return rawheight;
  }
//  /* ****************************************************** */
//  public double Get_Mote_Fire(Mote mote) {// virtual
//    double rawheight = mote.Get_Plane_Height();
//    return rawheight;// Globals.ActFun(rawheight);// key difference
//  }
//  /* ****************************************************** */
//  public double Get_Mote_Fire(int MoteNum) {// virtual
//    Mote mote = this.Motes.get(MoteNum);
//    return this.Get_Mote_Fire(mote);
//  }
  /* ****************************************************** */
  public void Gather_Correctors() {
    if (this.Num_Ds > 0) {// hack
      int Num_Motes = this.Motes.size();
      for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
        Mote cpnt = this.Motes.get(pcnt);
        cpnt.Gather_Correctors();
      }
    }
  }
  @Override public void Pass_Back_Corrector() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.Pass_Back_Corrector();
    }
  }
  public void Apply_Corrector() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.Apply_Corrector();
    }
    this.FitError = this.GetFitError();
    this.FitErrorFlywheel = this.FitErrorFlywheel * 0.99 + this.FitError * 0.01;
    if (false) {
      if (this.Explodable && this.FitErrorFlywheel > 0.25) {
        this.Explode();
//        Link ln;// to do: here we want to explode many of our neighbors too.
//        for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
//          Mote cpnt = this.Motes.get(pcnt);
////          ln - cpnt.DS[0].DS;
//        }
      }
    }
  }
  /* ****************************************************** */
  public double Get_Plane_Sigmoid_Height(PointNd pnt) {
    double hgt = this.planeform.Get_Plane_Height(pnt);
    hgt = Globals.ActFun(hgt);
    return hgt;
  }
  public void Ping() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      this.planeform.Ping(cpnt);
    }
  }
  public double GetFitError() {
    int NumPnts = Motes.size();
    Mote pnt;
    double Height, HgtDiff, Sum = 0.0, Avg, AvgRoot = 0.0;
    for (int pcnt = 0; pcnt < NumPnts; pcnt++) {
      pnt = Motes.get(pcnt);
      Height = planeform.Get_Plane_Height(pnt);
      Height = Globals.ActFun(Height);
      HgtDiff = Height - pnt.Get_Mapped_Outfire();
      Sum += HgtDiff * HgtDiff;
    }
    Avg = Sum / (double) NumPnts;
    AvgRoot = Math.sqrt(Avg);
    return AvgRoot;// Range should be 0.0 to 2.0, (if 2 is node box size).
  }
  public static int ExplodeCnt = 0;
  public String GetDate() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    return dtf.format(now);
  }
  public void Explode() {// do if node is stubbornly nonlinear
    int NumPnts = Motes.size();
    Mote pnt;// STUB, NOT FUNCTIONAL YET
    for (int pcnt = 0; pcnt < NumPnts; pcnt++) {
      pnt = Motes.get(pcnt);
      pnt.Randomize(-1.0, +1.0);
    }
    this.FitErrorFlywheel = 0.0;
    System.out.println("Explode! " + GetDate() + ", " + ExplodeCnt);
    ExplodeCnt++;
  }
  public NodeBox Clone_Me() {
    NodeBox child = new NodeBox();
    return child;
  }
}
