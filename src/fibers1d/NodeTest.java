package fibers1d;

import static fibers1d.Things.BoxSpacing;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeTest extends NodeBox {// stand alone bench test
  int Num_Dims = 3;
  /* ****************************************************** */
  public void Test() {
    this.Init_States(4);// must rethink this
    this.xorg = (1.0) * BoxSpacing;
    this.yorg = (1.0) * BoxSpacing;
    this.Init_And();
//    this.Init_Or();
//    this.Init_Same(-1.0);
//    this.Init_Same(1.0);
    this.planeform.Clear();
    this.PrintMotes();
    for (int cnt = 0; cnt < 1000; cnt++) {
      this.Ping();
      this.FitError = this.GetFitError();
      PrintPlane();
    }
    this.PrintMotes();
  }
  /* ****************************************************** */
  public void PrintMotes() {
    System.out.println("Motes");
    int Num_Motes = this.Motes.size();
    double Hgt;
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      Hgt = cpnt.Get_Height();
      System.out.print(Hgt + ", ");
    }
    System.out.println();
  }
  /* ****************************************************** */
  public void PrintPlane() {// find the heights of the corners of the plane
    System.out.println("Plane");
    double PlaneHgt;
    Mote cpnt = new Mote(this, Num_Dims);
    for (int ycnt = 0; ycnt < 2; ycnt++) {
      cpnt.loc[1] = (ycnt * 2.0) - 1.0;
      for (int xcnt = 0; xcnt < 2; xcnt++) {
        cpnt.loc[0] = (xcnt * 2.0) - 1.0;
        if (false) {
          PlaneHgt = this.planeform.Get_Plane_Height(cpnt);
        } else {
          PlaneHgt = this.Get_Plane_Sigmoid_Height(cpnt);
        }
        System.out.print(PlaneHgt + ", ");
      }
    }
    System.out.println();
  }
  /* ****************************************************** */
  @Override public void Pass_Back_Corrector() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.Apply_Height();
    }
  }
  /* ****************************************************** */
  @Override public void Ping() {
    int Num_Motes = this.Motes.size();
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      this.planeform.Ping(cpnt);
    }
  }
  /* ****************************************************** */
  @Override public NodeTest Clone_Me() {
    NodeTest child = new NodeTest();
    return child;
  }
}
